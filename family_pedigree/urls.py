from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path

from family_pedigree_site.views import (
    index_view,
    signin_view,
    signup_view,
    HomeView,
    editaccount_view,
    changepass_view,
    addmember_view,
    editmember_view,
    deletemember_view,
    signout_view,
)

urlpatterns = [
    path('', index_view, name='index'),
    path('signin/', signin_view, name='signin'),
    #path('signin/', auth_views.LoginView.as_view(template_name='signin.html',redirect_authenticated_user=True), name='signin'),
    path('signout/', signout_view, name='signout'),
    path('signup/', signup_view, name='signup'),
    path('homepage/', HomeView.as_view(), name='homepage'),
    path('editaccount/', editaccount_view, name='editaccount'),
    path('changepass/', changepass_view, name='changepass'),
    path('addmember/<int:account_id>/', addmember_view, name='addmember'),
    path('editmember/<int:member_id>/', editmember_view, name='editmember'),
    path('deletemember/<int:member_id>/', deletemember_view, name='deletemember'),
    path('admin/', admin.site.urls),
]
