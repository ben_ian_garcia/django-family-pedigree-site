import requests
import datetime

from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.views.generic.base import View

from family_pedigree_site.config import *
from .forms import SignInForm, SignUpForm, AddMemberForm, EditAccountForm, ChangePasswordForm


# Create your views here.
def index_view(request):
    return redirect("signin")

def signin_view(request):
    user = request.session.get("USER")
    if user:
        return redirect("homepage")

    error_message = None
    form = SignInForm()

    # REQUEST POST - Get user info
    if request.method == "POST":
        url = "{}/users/get_user/".format(api_url)

        request_body = request.POST
        username = request.session["SAVED_USERNAME"] = request_body.get("username")
        password = request_body.get("password")

        headers = construct_headers(request)
        payload = {
            "username": username,
            "password": password,
        }

        resource = requests.post(url, json=payload, headers=headers)
        resource_data = resource.json()

        # REQUEST POST - Get Account Info
        if resource.status_code == 200:
            user = request.session["USER"] = resource_data
            url = "{}/accounts/get_account/".format(api_url)
            user_id = resource_data["id"]
            payload = {
                "user_id" : user_id,
            }

            resource = requests.post(url, json=payload, headers=headers)
            resource_data = resource.json()

            if resource.status_code == 200:
                account = request.session["ACCOUNT"] = resource_data
                if user:
                    return redirect("homepage")
        else:
            status_code = resource_data.get("status")
            error_message = resource_data.get("message")
    saved_username = request.session.get("USER") or ''
    return render(request,
        template_name="signin.html",
        context={
            "form": form,
            "saved_username": saved_username,
            "error_message": error_message
    })

def signup_view(request):
    user = request.session.get("USER")
    if user:
        return redirect("homepage")
    error_message = None
    url = "{}/users/create_user/".format(api_url)
    form = SignUpForm()
    if request.method == "POST":
        request_body = request.POST
        username= request_body.get("username")
        password= request_body.get("password1")
        email= request_body.get("email")
        first_name= request_body.get("first_name")
        last_name= request_body.get("last_name")

        headers = construct_headers(request)
        payload = {
            "username" : username,
            "email" : email,
            "password" : password,
            "first_name" : first_name,
            "last_name" : last_name,
        }

        resource = requests.post(url, json=payload, headers=headers)
        resource_data = resource.json()

        if resource.status_code == 200:
            user = request.session["USER"] = resource_data
            url = "{}/accounts/create_account/".format(api_url)
            user_id = resource_data["id"]
            birthday= request_body.get("birthday")
            payload = {
                "user_id" : user_id,
                "birthday" : birthday,
            }

            resource = requests.post(url, json=payload, headers=headers)
            resource_data = resource.json()

            if resource.status_code == 200:
                if user:
                    return redirect("homepage")
        else:
            status_code = resource_data.get("status")
            error_message = resource_data.get("message")

    return render(request,
        template_name="signup.html",
        context={
            "form": form,
            "error_message": error_message,
    })

def editaccount_view(request):
    user = request.session.get("USER")
    if not user:
        return redirect("signin")

    user = request.session["USER"]
    username = user["username"]
    message = None

    if request.method == "POST":
        url = "{}/users/edit_user/".format(api_url)

        request_body = request.POST
        first_name= request_body.get("first_name")
        last_name= request_body.get("last_name")
        email= request_body.get("email")

        headers = construct_headers(request)
        payload = {
            "username": username,
            "first_name" : first_name,
            "last_name" : last_name,
            "email" : email,
        }

        resource = requests.post(url, json=payload, headers=headers)
        resource_data = resource.json()

        if resource.status_code == 200:
                user = request.session["USER"] = resource_data
                message = "User account successfully updated"

    # Initialize edit form
    f_name = user["first_name"]
    l_name = user["last_name"]
    email_add = user["email"]

    form = EditAccountForm(initial={
        "first_name": f_name,
        "last_name": l_name,
        "email" : email_add,
    })

    return render(request,
        template_name="editaccount.html",
        context={
            "form": form,
            "message": message,
    })

def changepass_view(request):
    user = request.session.get("USER")
    if not user:
        return redirect("signin")

    message = None
    user = request.session["USER"]
    username = user["username"]
    form = ChangePasswordForm()
    if request.method == "POST":
        url = "{}/users/change_pass/".format(api_url)

        request_body = request.POST
        old_password= request_body.get("old_password")
        new_password1= request_body.get("new_password1")
        new_password2= request_body.get("new_password2")

        headers = construct_headers(request)
        payload = {
            "username": username,
            "old_password" : old_password,
            "new_password1" : new_password1,
            "new_password2" : new_password2,
        }

        resource = requests.post(url, json=payload, headers=headers)
        resource_data = resource.json()

        if resource.status_code == 200:
            message = "Password successfully changed!"
        else:
            message = resource_data["message"]

    return render(request,
        template_name="changepass.html",
        context={
            "form": form,
            "message": message,
    })

def authorization(request):
    url = "{}/o/token/".format(api_url)

    payload = {
        "grant_type": grant_type,
        "username": client_user,
        "password": client_password,
    }

    auth = (client_id, client_secret)

    resource = requests.post(url, json=payload, auth=auth)
    resource_data = resource.json()

    request.session["API_SESSION"] = resource_data
    access_token = request.session["API_SESSION"]["access_token"]

    return access_token

def construct_headers(request):
    access_token = authorization(request)
    return { "Authorization": "Bearer " + access_token}

class HomeView(View):
    template_name = "homepage.html"

    def get(self, request):
        user = request.session.get("USER")
        if not user:
            return redirect("signin")
        account = request.session.get("ACCOUNT")

        # GET Member List
        url = "{}/members/".format(api_url)

        headers = construct_headers(request)
        payload = {
            "account_id": user["id"],
        }

        resource = requests.get(url, json=payload, headers=headers)
        resource_data = resource.json()

        member = resource_data

        if user:
            context = {
                "user": user,
                "account": account,
                "member": member,
            }
            return render(request, template_name=self.template_name,context=context)
        else:
            return redirect("signin")

def addmember_view(request, account_id):
    user = request.session.get("USER")
    if not user:
        return redirect("signin")

    form = AddMemberForm()
    account = request.session.get("ACCOUNT")
    acc_id = account["id"]
    if request.method == "POST":
        url = "{}/members/add_member/".format(api_url)
        request_body = request.POST
        relationship= request_body.get("relationship")
        first_name= request_body.get("first_name")
        last_name= request_body.get("last_name")
        birthday= request_body.get("birthday")
        vital_status= request_body.get("vital_status")

        headers = construct_headers(request)
        payload = {
            "account_id": acc_id,
            "relationship" : relationship,
            "first_name" : first_name,
            "last_name" : last_name,
            "birthday" : birthday,
            "vital_status" : vital_status,
        }

        resource = requests.post(url, json=payload, headers=headers)
        resource_data = resource.json()

        if resource.status_code == 200:
                return redirect("homepage")

    return render(request,
        template_name="addmember.html",
        context={
            "form":form,
            "format": "Add",
        })

def editmember_view(request, member_id):
    user = request.session.get("USER")
    if not user:
        return redirect("signin")

    account = request.session.get("ACCOUNT")
    account_id = account["id"]
    member_id =  member_id

    if request.method == "POST":
        url = "{}/members/edit_member/".format(api_url)

        request_body = request.POST
        relationship= request_body.get("relationship")
        first_name= request_body.get("first_name")
        last_name= request_body.get("last_name")
        birthday= request_body.get("birthday")
        birthday_format = str(datetime.datetime.strptime(birthday, "%b %d, %Y").date())
        vital_status= request_body.get("vital_status")

        headers = construct_headers(request)
        payload = {
            "account_id": account_id,
            "member_id": member_id,
            "relationship" : relationship,
            "first_name" : first_name,
            "last_name" : last_name,
            "birthday" : birthday_format,
            "vital_status" : vital_status,
        }

        resource = requests.post(url, json=payload, headers=headers)
        resource_data = resource.json()

        if resource.status_code == 200:
                return redirect("homepage")

    else:
        url = "{}/members/get_member/".format(api_url)

        headers = construct_headers(request)
        payload = {
            "account_id": account_id,
            "member_id": member_id,
        }

        resource = requests.post(url, json=payload, headers=headers)
        resource_data = resource.json()

        if resource.status_code == 200:
            relationship = resource_data["relationship"]
            rel_id = resource_data["rel_id"]
            first_name = resource_data["first_name"]
            last_name = resource_data["last_name"]
            birthday = resource_data["birthday"]
            vital_status = resource_data["vital_status"]
            vs_id = resource_data["vs_id"]

            form = AddMemberForm(
                initial={
                    "relationship": rel_id,
                    "first_name": first_name,
                    "last_name": last_name,
                    "birthday": birthday,
                    "vital_status": vs_id,
                })

    return render(request,
        template_name="addmember.html",
        context={
            "form":form,
            "format": "Edit",
        })

def deletemember_view(request, member_id):
    user = request.session.get("USER")
    if not user:
        return redirect("signin")

    relationship = ""
    last_name = ""
    first_name = ""
    birthday = ""
    vital_status = ""

    account = request.session.get("ACCOUNT")
    account_id = account["id"]
    member_id =  member_id
    if request.method == "POST":
        url = "{}/members/delete_member/".format(api_url)

        headers = construct_headers(request)
        payload = {
            "account_id": account_id,
            "member_id": member_id,
        }

        resource = requests.post(url, json=payload, headers=headers)
        resource_data = resource.json()

        if resource.status_code == 200:
            return redirect("homepage")
    else:
        url = "{}/members/get_member/".format(api_url)

        headers = construct_headers(request)
        payload = {
            "account_id": account_id,
            "member_id": member_id,
        }

        resource = requests.post(url, json=payload, headers=headers)
        resource_data = resource.json()

        if resource.status_code == 200:
            relationship = resource_data["relationship"]
            first_name = resource_data["first_name"]
            last_name = resource_data["last_name"]
            birthday = resource_data["birthday"]
            vital_status = resource_data["vital_status"]

    return render(request,
        template_name="deletemember.html",
        context={
            "relationship": relationship,
            "first_name": first_name,
            "last_name": last_name,
            "birthday": birthday,
            "vital_status": vital_status,
        })

def signout_view(request):
    request.session.flush()
    return render(request,
        template_name="signout.html"
    )