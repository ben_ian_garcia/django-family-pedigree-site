from django.apps import AppConfig


class FamilyPedigreeSiteConfig(AppConfig):
    name = 'family_pedigree_site'
