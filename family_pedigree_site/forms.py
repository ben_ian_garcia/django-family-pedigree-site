from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, PasswordChangeForm

VITAL_STATUS_CHOICES = (
    ('A', 'Alive'),
    ('D', 'Deceased'),
    ('U', 'Unknown'),
)

class SignInForm(forms.Form):
    username = forms.CharField(label="", max_length=50,
                               widget=forms.TextInput(
                                   attrs={'placeholder': 'Username'}))
    password = forms.CharField(label="",widget=forms.PasswordInput(
                                    attrs={'placeholder': 'Password'}))


class SignUpForm(UserCreationForm):
    birthday = forms.DateField(widget=forms.TextInput(attrs={'placeholder': 'Birthday'}))

    class Meta:
        model = User
        fields =['username', 'password1', 'password2', 'email', 'first_name', 'last_name', 'birthday']
        widgets ={
            'username': forms.TextInput(attrs={'placeholder': 'Username'}),
            'password': forms.PasswordInput(attrs={'placeholder': 'Password'}),
            'email': forms.TextInput(attrs={'placeholder': 'Email'}),
            'first_name': forms.TextInput(attrs={'placeholder': 'First Name'}),
            'last_name': forms.TextInput(attrs={'placeholder': 'Last Name'}),
        }


class EditAccountForm(forms.Form):
    first_name = forms.CharField(label="First Name", max_length=30)
    last_name = forms.CharField(label="Last Name", max_length=30)
    email = forms.CharField(label="Email", max_length=30)
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email']

class ChangePasswordForm(forms.Form):
    old_password = forms.CharField(widget=forms.PasswordInput())
    new_password1 = forms.CharField(label="New Password", widget=forms.PasswordInput())
    new_password2 = forms.CharField(label="Confirm Password", widget=forms.PasswordInput())

    class Meta:
        fields =['old_password', 'new_password1', 'new_password2']

class AddMemberForm(forms.Form):
    RELATIONSHIP_CHOICES = (
        ("0", ' '),
        ("1", 'Father'),
        ("2", 'Mother'),
        ("3", 'Brother'),
        ("4", 'Sister'),
        ("5", 'Son'),
        ("6", 'Daughter'),
        ("7", 'Spouse'),
        ("8a", 'Paternal Grandfather'),
        ("8b", 'Maternal Grandfather'),
        ("9a", 'Paternal Grandmother'),
        ("9b", 'Maternal Grandmother'),
        ("10", 'In-law'),
        ("11", 'Grandchild'),
        ("12", 'In-law'),
        ("13a", 'Aunt'),
        ("13b", 'Uncle'),
    )

    relationship = forms.ChoiceField(choices = RELATIONSHIP_CHOICES, initial=" ", widget=forms.Select(attrs={'class':'dropdown'}))
    first_name = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'First Name'}))
    last_name = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Last Name'}))
    birthday = forms.DateField(widget=forms.TextInput(attrs={'placeholder': 'Birthday'}))
    vital_status = forms.ChoiceField(choices = VITAL_STATUS_CHOICES, widget=forms.Select(attrs={'class':'dropdown'}))

    class Meta:
        fields = ['relationship', 'first_name', 'last_name', 'birthday', 'vital_status']